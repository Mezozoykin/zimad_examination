package zimad.examination.ui;

/**
 * Created by Sasha on 21.01.2018.
 */

public class DogFragment extends BaseFragment {
    @Override
    protected String getPetType() {
        return "dog";
    }
}
