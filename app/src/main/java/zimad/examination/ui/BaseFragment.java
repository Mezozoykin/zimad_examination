package zimad.examination.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import zimad.examination.BR;
import zimad.examination.ExaminationApp;
import zimad.examination.R;
import zimad.examination.adapters.RecyclerBindingAdapter;
import zimad.examination.databinding.FragmentMasterBinding;
import zimad.examination.model.Pet;
import zimad.examination.model.PetsResponse;

/**
 * Created by Sasha on 21.01.2018.
 */

public abstract class BaseFragment extends Fragment implements RecyclerBindingAdapter.OnItemClickListener<Pet> {
    private static final String LIST_KEY = "LIST", TAG = "DETAIL", LIST_STATE_KEY = "LIST_STATE_KEY";
    protected abstract String getPetType();
    protected RecyclerBindingAdapter<Pet> adapter;
    protected FragmentMasterBinding binding;
    protected List<Pet> petList = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
      binding = DataBindingUtil.inflate(inflater, R.layout.fragment_master,  container, false);
      adapter = new RecyclerBindingAdapter<>(R.layout.item_pet, BR.pet, petList);
      adapter.setOnItemClickListener(this);
      linearLayoutManager = new LinearLayoutManager(getActivity());
      binding.rv.setLayoutManager(linearLayoutManager);
      binding.rv.setAdapter(adapter);

      Bundle bundle;
      if (savedInstanceState != null) {
          bundle = savedInstanceState;
      } else {
          bundle = getArguments();
      }

      if (bundle != null) {
          ArrayList<Pet> savedList = bundle.getParcelableArrayList(LIST_KEY);
          if (savedList != null) {
              petList = savedList;
              adapter.setItems(petList);
              Parcelable linearLayoutManagerState = bundle.getParcelable(LIST_STATE_KEY);
              if (linearLayoutManagerState != null) linearLayoutManager.onRestoreInstanceState(linearLayoutManagerState);
          }
      }

      if (petList.size() == 0) request();

      return binding.getRoot();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(LIST_KEY, new ArrayList<>(petList));
        if (linearLayoutManager != null) outState.putParcelable(LIST_STATE_KEY, linearLayoutManager.onSaveInstanceState());
    }

    private void request() {
        ExaminationApp.getApp().getNetApi().getPets(getPetType())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(this::processResponse, this::processError);
    }

    private void processResponse(PetsResponse petsResponse) {
        petList = petsResponse.getData();
        adapter.setItems(petList);
    }

    private void processError(Throwable throwable) {
        Toast.makeText(getActivity(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(int position, Pet item) {
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.mainContainer, DetailFragment.getInstance(item), TAG)
                .addToBackStack(null)
                .commit();
    }


}
