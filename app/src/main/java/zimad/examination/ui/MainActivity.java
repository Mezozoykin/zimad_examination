package zimad.examination.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import zimad.examination.R;
import zimad.examination.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {
    private static final String TAB_KEY = "TAB", HIDDEN_TAB = "HIDDEN_TAB";
    ActivityMainBinding binding;
    private int currentTab = 0;
    private BaseFragment[] petFragments = new BaseFragment[2];
    private Bundle stateHiddenFragment = new Bundle();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding =  DataBindingUtil.setContentView(this, R.layout.activity_main);

        if (savedInstanceState != null) {
            currentTab = savedInstanceState.getInt(TAB_KEY);
            TabLayout.Tab tab = binding.tabLayout.getTabAt(currentTab);
            if (tab != null) tab.select();
            petFragments[currentTab] = (BaseFragment)getSupportFragmentManager().findFragmentByTag(String.valueOf(currentTab));
            int tabToRestore = currentTab == 0 ? 1 : 0;
            petFragments[tabToRestore] = currentTab == 0 ? new DogFragment() : new CatFragment();
            petFragments[tabToRestore].setArguments(savedInstanceState.getBundle(HIDDEN_TAB));
        } else {
            petFragments[0] = new CatFragment();
            petFragments[1] = new DogFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frameContainer, petFragments[0], String.valueOf(currentTab))
                    .commit();
        }

        binding.tabLayout.addOnTabSelectedListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(TAB_KEY, currentTab);
        int tabToSave = currentTab == 0 ? 1 : 0;
        petFragments[tabToSave].onSaveInstanceState(stateHiddenFragment);
        outState.putBundle(HIDDEN_TAB, stateHiddenFragment);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    public void showPetFragment() {
        getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.frameContainer, getAttachFragment(), String.valueOf(currentTab))
            .commit();
    }

    private Fragment getAttachFragment(){
        return petFragments[currentTab];
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        currentTab = tab.getPosition();
        showPetFragment();
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

}
