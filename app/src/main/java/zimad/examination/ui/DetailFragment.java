package zimad.examination.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import zimad.examination.R;
import zimad.examination.databinding.FragmentDetailBinding;
import zimad.examination.model.Pet;

/**
 * Created by Sasha on 21.01.2018.
 */

public class DetailFragment extends Fragment {
    private static final String PET_KEY = "PET";
    FragmentDetailBinding binding;
    private Pet pet;

    public static DetailFragment getInstance(Pet pet) {
        DetailFragment detailFragment = new DetailFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(PET_KEY, pet);
        detailFragment.setArguments(bundle);
        return detailFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_detail,  container, false);
        Bundle bundle;
        if (savedInstanceState != null) {
            bundle = savedInstanceState;
        } else {
            bundle = getArguments();
        }
        if (bundle != null) {
            pet = bundle.getParcelable(PET_KEY);
            binding.setPet(pet);
        }
        return binding.getRoot();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(PET_KEY, pet);
    }
}
