package zimad.examination.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.databinding.BindingMethod;
import android.databinding.BindingMethods;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.nostra13.universalimageloader.core.ImageLoader;

import zimad.examination.R;

/**
 * Created by Sasha on 21.01.2018.
 */

@BindingMethods({
        @BindingMethod(type = ImageLoaderView.class, attribute = "imageUrl", method = "loadImage")})
public class ImageLoaderView extends AppCompatImageView{
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ImageLoaderView(Context context) {
        super(context);
    }

    public ImageLoaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageLoaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.obtainStyledAttributes(attrs
                , R.styleable.ImageLoaderView, defStyleAttr, 0);
        loadImage(
                typedArray.getString(R.styleable.ImageLoaderView_imageUrl)
        );
        typedArray.recycle();
    }


    public void loadImage(String imageUrl) {
        if (imageUrl != null) {
            setUrl(imageUrl);
            ImageLoader.getInstance().displayImage(imageUrl, this);
        }
    }
}

