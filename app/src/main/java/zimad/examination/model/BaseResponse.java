package zimad.examination.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sasha on 20.01.2018.
 */


public abstract class BaseResponse<T> {

    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<T> data;

    public BaseResponse(List<T> data) {
        this.data = data;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
