package zimad.examination.model;

import java.util.ArrayList;

/**
 * Created by Sasha on 20.01.2018.
 */

public class PetsResponse extends BaseResponse<Pet> {
    public PetsResponse(ArrayList<Pet> data) {
        super(data);
    }
}
