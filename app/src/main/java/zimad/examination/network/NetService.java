package zimad.examination.network;

import com.google.gson.Gson;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Sasha on 20.01.2018.
 */

public class NetService {

    public static NetApi getNetApi() {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addNetworkInterceptor(logging);

        httpClient.addInterceptor(chain -> {


            Request original = chain.request();

            Request.Builder request = original.newBuilder();

            request.header("Content-Type", "application/json");

            return chain.proceed(request.build());
        });


        Gson gson = new Gson();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(NetApi.URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build();

        return retrofit.create(NetApi.class);
    }


}
