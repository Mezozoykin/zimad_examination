package zimad.examination.network;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;
import zimad.examination.model.PetsResponse;

/**
 * Created by Sasha on 20.01.2018.
 */

public interface NetApi {
    String URL = "http://kot3.com/xim/";

    @GET("api.php")
    //String getPets(@Query("query") String petType);
    Single<PetsResponse> getPets(@Query("query") String petType);

}