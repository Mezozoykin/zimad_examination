package zimad.examination;

import android.app.Application;
import android.graphics.Bitmap;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import zimad.examination.network.NetApi;
import zimad.examination.network.NetService;

/**
 * Created by Sasha on 20.01.2018.
 */

public class ExaminationApp extends Application {
    private NetApi netApi;
    private static ExaminationApp app;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        initUniversalImageLoader();
        netApi = NetService.getNetApi();
    }

    public static ExaminationApp getApp() {
        return app;
    }

    public NetApi getNetApi() {
        return netApi;
    }

    private void initUniversalImageLoader() {

        DisplayImageOptions imageLoaderDefaultDisplayOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .considerExifParams(true)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(imageLoaderDefaultDisplayOptions)
                .build();

        ImageLoader.getInstance().init(config);
    }
}
